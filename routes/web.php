<?php

 // Route::get('/infophp', function()
 // {
 // 	phpinfo();
 // });


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('home');

Route::post('/news', 'PublicController@create')->name('createNews');
Route::get('/news/{id}', 'PublicController@show')
	->name('showNews')
	->where('id', '[0-9]+');


Route::get('/news/edit/{id}', 'PublicController@edit')
	->name('editNews')
	->where('id', '[0-9]+');

Route::put('/news/edit/{id}', 'PublicController@update')
	->name('updateNews')
	->where('id', '[0-9]+');


Route::patch('/news/heart/{id}', 'PublicController@addHeart')
	->name('heart')
	->where('id', '[0-9]+');


Route::delete('/news/delete/{id}', 'PublicController@delete')
	->name('deleteNews')
	->where('id', '[0-9]+');


Route::get('/user', 'PublicController@userhome')->name('user');

Route::get('/news/{id}/contact', 'PublicController@contactUser')->name('contactUser');



Route::get('/post/create', 'PublicController@createPost')->name('createPost');

Route::post('/post', 'PublicController@storePost')->name('storePost');



Route::get('/welcome', 'PublicController@createEmail')->name('createEmail');

Route::post('/welcome', 'PublicController@sendEmail')->name('sendEmail');


Route::post('/news/{id}/tags', 'PublicController@addTags')->name('addTags');

Route::get('/tags/{id}', 'PublicController@showTags')->name('showTags');

Route::delete('/news/{id}/tags-delete', 'PublicController@deleteTags')->name('deleteTags');




Auth::routes();

Route::get('/home', 'HomeController@index');
