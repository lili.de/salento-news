 <form method="POST" action="{{ route('updateNews', $news->id) }} " enctype="multipart/form-data">
  @method('PUT')
  @csrf
  <div class="form-group row">
    <label for="inputTitle" class="col-sm-2 col-form-label">Titolo</label>
    <div class="col-sm-10">
      <input type="text" name="title" value="{{ $news->title }}" class="form-control" id="inputTitle" placeholder="Titolo">
    </div>
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Articolo</label>
    <textarea class="form-control" name="body" value="{{ $news->body }}" id="inputMessage" rows="3">{{ $news->body }}</textarea>
  </div>

  <div class="form-group">
    <label for="inputImage">Immagine</label>
    <input type="file" name="img" class="form-control" value="{{ $news->img }}" id="inputImage" placeholder="Scegli immagine">
  </div>

  <br><br>

  <div class="form-group row">
    <label for="inputAlt" class="col-sm-2 col-form-label">Alt Immagine</label>
    <div class="col-sm-10">
      <input type="text" name="alt" class="form-control" value="{{ $news->alt }}" id="inputAlt" placeholder="Alt Immagine">
    </div>
  </div>

  <br><br> 

  <button type="submit" class="btn btn-info">Aggiungi notizia</button>

</form>