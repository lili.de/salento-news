@extends('layouts.app')

@section('content')
<div class="container">
    <br>

    @if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
    @endif
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Notizia n. {{ $news->id }}</h2></div>


                <div class="card-body">

                    <h3>{{ $news->title }}</h3>
                    <br>
                    <p>{{ $news->body }}</p>
                    <br>
                    <img src="{{ $news->img }}" alt="{{ $news->alt }}" class="img-fluid" style="max-width: 100%">
                    <br><br>
                    <p>
                        Created by {{ $news->user->name }}

                        <a href="{{ route ('contactUser', $news->id) }}" style="float: right; display: inline-flex;">Contatta l'autore</a>
                    </p>

                    <br>

                    <form action="{{ route('addTags', [$news->id]) }}" method="POST">
                        @csrf
                        Scegli tag : 

                        <select name="tag_id" class="custom-select custom-select-sm">

                            @foreach($tags as $tag)

                            <option value="{{ $tag->id }}">{{ $tag->tag }}  {{ $tag->icon }}</option>

                            @endforeach

                        </select>

                        <button type="input" class="btn btn-light" style="float: right;padding-left: 2px; padding-right: 2px; padding-bottom: 1px; padding-top: 1px; font-size: 0.7rem;"><span>AGGIUNGI</span></button>
                    </form>

                </div>
                <br>
                <div class="card-header">

                    @foreach($news->tags as $tag)

                    <span><a href="{{ route('showTags',[$tag->id])}}">{{ $tag->tag }}</a> {{ $tag->icon }}&nbsp;
                        <form action="{{ route('deleteTags',[$news->id]) }}" method="POST" style="display: inline-flex;">
                            @method('DELETE')
                            @csrf
                            <button name="tag_id" type="input" value="{{ $tag->id }}" class="btn btn-transparent" style="padding: 0; background: none;color: none;"><span>| ❌ | </span></button>
                        </form>&nbsp;&nbsp;</span>

                        @endforeach

                    </div>
                    <br>
                    <div class="card-header"><a href="{{ route ('home') }}" class="btn btn-outline-info" style="padding-left: 0.5px; padding-right: 0.5px; padding-bottom: 0px; padding-top: 0px; font-size: 0.7rem;"><span>TORNA IN HOME</span></a></div>
                </div>
            </div>
        </div>

    </div>

    @endsection
