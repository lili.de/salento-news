@extends('layouts.app')

@section('content')
<div class="container">
    <br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Notizia n. {{ $news->id }}</h2></div>

                <div class="card-body">

                    @include('news._news-edit')

                </div>
                
            </div>
        </div>
    </div>

</div>

@endsection
