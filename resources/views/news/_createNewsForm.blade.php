
@if ($errors->any())

<br><br>
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
<br><br>

@endif

 <form method="POST" action="{{ route('createNews') }}" enctype="multipart/form-data">
  @csrf
  <div class="form-group row">
    <label for="inputTitle" class="col-sm-2 col-form-label">Titolo</label>
    <div class="col-sm-10">
      <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Titolo" value="{{  old('title') }}">
    </div>
  </div>

  <div class="form-group">
    <label for="inputMessage">Articolo</label>
    <textarea class="form-control" name="body" id="inputMessage" rows="3" Placeholder="Scrivi qui il tuo articolo">{{  old('body')}}</textarea>
  </div>

  <div class="form-group">
    <label for="inputImage">Immagine</label>
    <input type="file" name="img" class="form-control" id="inputImage" placeholder="Scegli immagine" value="{{  old('img') }}">
  </div>

  <br><br>

  <div class="form-group row">
    <label for="inputAlt" class="col-sm-2 col-form-label">Alt Immagine</label>
    <div class="col-sm-10">
      <input type="text" name="alt" class="form-control" id="inputAlt" placeholder="Alt Immagine" value="{{  old('alt') }}">
    </div>
  </div>

  <br><br> 

  <button type="submit" class="btn btn-info">Aggiungi notizia</button>

</form>