 <form method="POST" action="{{ route('sendEmail') }}">
  @csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-2 col-form-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="inputName" placeholder="Nome" value="{{ old('name')}}">
    </div>
  </div>

  <div class="form-group">
    <label for="inputMessage">Messaggio</label>
    <textarea class="form-control" name="body" id="inputMessage" rows="3" Placeholder="Messaggio">{{  old('body')}}</textarea>
  </div>

  <br><br> 

  <button type="submit" class="btn btn-info">Invia</button>

</form>