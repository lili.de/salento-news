@component('mail::message')
# Introduction

Ciao {{ $email['name'] }} !

Grazie per esserti registrato!

Il tuo messaggio è :

{{ $email['body'] }}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
