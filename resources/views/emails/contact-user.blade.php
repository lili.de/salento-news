<!DOCTYPE html>
<html>
<head>
	<title>Messaggio diretto</title>
</head>
<body>
	<h2>Ciao {{ $news->user->name }}</h2>
	<br>
	<p>Ti contatto perchè mi interessa il tuo articolo</p>
	<br>
	<h4>Titolo : <blockquote>{{ $news->title }}</blockquote></h4>
	<p>Articolo : <blockquote>{{ $news->body }}</blockquote></p>
</body>
</html>