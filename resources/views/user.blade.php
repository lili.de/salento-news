@extends('layouts.app')

@section('content')
<div class="container">
    <br>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Ciao {{ Auth::user()->name }}</h2><br><h3>Ecco le tue news : </h3></div>

                <div class="card-body">

                    @foreach($userNews as $n)

                    {{ $loop->index+1 }} 🏄‍♂️ <a href="{{ route('showNews', [$n->id]) }}">{{ $n->title}}</a>

                    <br><br>

                    <div class="col">

                        <div style="float: left; display: inline-flex;"><form action="{{ route('heart', [$n->id]) }}" method="POST">
                            @method('PATCH')
                            @csrf
                            <button type="input" class="btn btn-transparent" style="padding: 0; background: none; color: none">♥️</button>
                        </form><span style="opacity: 0.6">&nbsp;&nbsp;{{$n->heart}}</span></div>

                    </div>

                    <div class="col">

                        <div style="float: right; display: inline-flex;">

                            <a href="{{ route('showNews', [$n->id]) }}" class="btn btn-outline-success" style="padding-left: 2px; padding-right: 2px; padding-bottom: 1px; padding-top: 1px; font-size: 0.7rem;"><span>LEGGI</span></a>&nbsp;

                            <a href="{{ route('editNews', [$n->id]) }}" class="btn btn-outline-warning" style="padding-left: 2px; padding-right: 2px; padding-bottom: 1px; padding-top: 1px; font-size: 0.7rem;"><span>MODIFICA</span></a>&nbsp;

                            <form action="{{ route('deleteNews', [$n->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="input" class="btn btn-outline-danger" style="padding-left: 2px; padding-right: 2px; padding-bottom: 1px; padding-top: 1px; font-size: 0.7rem;"><span>CANCELLA</span></button>
                            </form>

                        </div>

                    </div>

                    <br><hr><br>

                    @endforeach


                </div>
                <div class="card-header"><a href="{{ route ('home') }}" class="btn btn-outline-info" style="padding-left: 0.5px; padding-right: 0.5px; padding-bottom: 0px; padding-top: 0px; font-size: 0.7rem;"><span>TORNA IN HOME</span></a></div>
            </div>
        </div>
    </div>

</div>

@endsection
