 <form method="POST" action="{{ route('storePost') }}">
  @csrf
  <div class="form-group row">
    <label for="inputTitle" class="col-sm-2 col-form-label">Titolo</label>
    <div class="col-sm-10">
      <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Titolo" value="{{ old('title')}}">
    </div>
  </div>

  <div class="form-group">
    <label for="inputMessage">Post</label>
    <textarea class="form-control" name="body" id="inputMessage" rows="3" Placeholder="Scrivi qui il tuo articolo">{{  old('body')}}</textarea>
  </div>

  <br><br> 

  <button type="submit" class="btn btn-info">Aggiungi Post</button>

</form>