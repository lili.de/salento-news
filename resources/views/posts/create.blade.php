@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Salento News</div>

                <div class="card-body">

                    @guest

                    <h2>Benvenuto! Registrati per inserire i tuoi post!</h2>

                    @else

                    <h2>Bentornato {{ Auth::user()->name }}!</h2>
                    <br><br>


                </div>
            </div>
        </div>
    </div>

    <br><br>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>Inserisci i tuoi post!</h3></div>

                <div class="card-body">

                    @include('posts._createPostForm')

                </div>
            </div>
        </div>
    </div>

    @endguest

    <br><br>



    <div class="row justify-content-center">
        <div class="col- @guest 12 @else md-8 @endguest">
            <div class="card">
                <div class="card-header">Ultimi Post</div>

                <div class="card-body">


                    @ include('posts.all-posts')


                </div>
            </div>
        </div>
    </div>


</div>


@endsection
