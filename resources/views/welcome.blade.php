@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Salento News</div>

                <div class="card-body">

                    @guest

                    <h2>Benvenuto! Registrati per inserire le tue news!</h2>

                    @else

                    <h2>Bentornato {{ Auth::user()->name }}!</h2>
                    <br><br>


                </div>
            </div>
        </div>
    </div>

    <br><br>

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>Inserisci le tue news!</h3></div>

                <div class="card-body">

                    @include('news._createNewsForm')

                </div>
            </div>
        </div>
    </div>

                    @endguest

    <br><br>



    <div class="row justify-content-center">
        <div class="col- @guest 12 @else md-8 @endguest">
            <div class="card">
                <div class="card-header">Ultime News</div>

                <div class="card-body">


                    @include('news.all-news')


                </div>
            </div>
        </div>
    </div>


</div>


@endsection
