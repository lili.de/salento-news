@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">News con il tag&nbsp;&nbsp;<span class="font-weight-bold" style="font-size: 1.3rem;">{{ $tag->tag }}&nbsp;&nbsp;{{ $tag->icon }}</span></div>
                
                <div class="card-body">

                    @foreach ($news as $n)

                        🏄‍♂️  <a href="{{ route('showNews', [$n->id]) }}">{{ $n->title}}</a>
                        <br>
                        <hr>

                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection
