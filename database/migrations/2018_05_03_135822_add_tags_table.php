<?php

use App\Tag;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->string('icon');
            $table->timestamps();
        });

        Tag::create(['tag'=>'Sun','icon'=>'☀️']);
        Tag::create(['tag'=>'Sea','icon'=>'🏖']);
        Tag::create(['tag'=>'Party','icon'=>'🍹']);
        Tag::create(['tag'=>'Sport','icon'=>'🏊‍♀️']);
        Tag::create(['tag'=>'Love','icon'=>'♥️']);
        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('tags');

    }
}
