<?php

namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\StoreBlogNews;
use App\Http\Requests\StoreBlogPost;
use App\Jobs\ResizeNewsImage;
use App\Jobs\SendWelcomeEmail;
use App\Mail\ContactUser;
use App\Mail\WelcomeEmail;
use App\News;
use App\Post;
use App\Tag;
use Folklore\Image\Facades\Image;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;



class PublicController extends Controller
{
    

	public function index(Request $req)

	{
		
		$news = News::orderBy('id','desc')->get();
		return view('welcome',['news'=>$news]);

	}

	public function create(StoreBlogNews $req)

	{

		// $title = $req->input('title');
		// $body = $req->input('body');
		$validated = $req->validated();

		$img_url = $req->file('img')->store('public/userfiles');
		$img_url = Storage::url($img_url);

		
		dispatch(new ResizeNewsImage($img_url));



		$news = new News;
		$news->title = $req->title;
		$news->body = $req->body;
		$news->img = $img_url;
		 
		if (!empty($alt)) {

			$news->alt = $req->alt;
			
		}
		
		$news->heart = $req->heart;

		$news->user_id = Auth::user()->id;


		$news->save();

		return redirect('/')->with('status', 'News created!');

	}

	public function show($id)

	{

		$news = News::find($id);
		$tags = Tag::all();

		return view('news.detail',['news'=>$news],['tags'=>$tags]);

	}


	public function edit($id)

	{


		if (Auth::user()) {

			$news = News::find($id);

			return view('news.edit',['news'=>$news]);

		}else{

			return abort('404');
		}



	}

	public function update(Request $req, $id)

	{
		$img_url = $req->file('img')->store('public/userfiles');
		$img_url = Storage::url($img_url);

		$news = News::find($id);
		$news->title = $req->title;
		$news->body = $req->body;
		$news->img = $img_url;
		if (!empty($alt)) {

			$news->alt = $req->alt;
			
		}
		
		$news->heart = $req->heart;

		$news->save();

		return redirect('/');
		
	}



	public function delete(Request $req, $id)

	{
		if (Auth::user()) {

			News::destroy($id);

		}else{

			return abort('404');
		}

		// $news = News::find($id);
		// $news->delete();
		// $news->splice();

		// $news->save();

			return redirect('/');

		}


	public function addHeart(Request $req, $id)

	{
		
		$news = News::find($id);

		$news->heart ++;

		$news->save();

		return redirect('/');

	}


	public function userhome ()
	{

		$currentUser = Auth::user();
		$userNews = $currentUser->news()->orderBy('id','desc')->get();


		return view('user',['userNews'=>$userNews]);
	}


	public function contactUser(Request $req, $id)
	{

		$news = News::findOrFail($id);

		Mail::to($news->user->email)->send(new ContactUser($news));

		return redirect('/');

	}


	public function createPost ()
	{

		$posts = Post::orderBy('id','desc')->get();
		return view('posts.create',['posts'=>$posts]);

	}

	public function storePost(StoreBlogPost $req)
    {
        // Validate and store the blog post...

     //    $validatedData = $req->validate([

     //    'title' => 'required|unique:posts|max:10',
     //    'body' => 'required',

    	// ]);

    	$validated = $req->validated();

        $posts = new Post;
		$posts->title = $req->title;
		$posts->body = $req->body;
		$posts->save();

		return redirect('/')->with('status', 'Post created!');

    }


    public function createEmail()
    {

    	return view('emails.create');

    }

    public function sendEmail(Request $req)
    {
    	$email = $req->except('_token');
    	
    	SendWelcomeEmail::dispatch($email);
    	return redirect('/')->with('status', 'Email sent!');
    }


    public function addTags(Request $req, $id)
    {

    	// dd($id, $req->input('tag_id'));

    	$news = News::find($id);

    	// $news->has('tags', '=', $req->input('tag_id'))->get();
    	$q = $news->tags()->find($req->input('tag_id'));

    	if ($q != null){

    		return redirect(route('showNews',[$id]))->with('status','Non puoi aggiungere lo stesso tag!');

    	}

    	$news->tags()->attach($req->input('tag_id'));

    	return redirect(route('showNews',[$id]));

    }


    public function showTags($id)

	{

		$tag = Tag::find($id);
		$news = $tag->news()->get();

		return view('tags.detail',['news'=>$news],['tag'=>$tag]);

	}


	public function deleteTags(Request $req, $id)
    {

    	// dd($id, $req->input('tag_id'));
    	$news = News::find($id);
    	$news->tags()->detach($req->input('tag_id'));

    	return redirect(route('showNews',[$id]));

    }


}
