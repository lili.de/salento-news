<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        /* By default it returns false, change it to 
         * something likethis if u are checking
         * authentication
         */
        return Auth::check();

        /* Or something more granular like this: 
         * return auth()->user()->can('udpate-profile')
         * if you are using the Laratrust package.
         */
    }


    public function rules()
    {
        return [
            'title' => 'required|unique:posts|max:25',
            'body' => 'required',
        ];
    }


}
