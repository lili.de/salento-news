<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    
	protected $fillable = [
        'tag', 'icon',
    ];


    public function news()

    {
        return $this->belongsToMany('App\News');
    }

}
