<?php

namespace App\Jobs;

use Folklore\Image\Facades\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResizeNewsImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

protected $img_url;  
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($img_url)
    {
        
        $this->img_url = $img_url;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        // Image::url('public/userfiles',800,300,array('crop'));
        // ----------------------

        Image::make($this->img_url,array(

            'width' => 800,
            'height' => 400,
            'crop' => true

        ))->save(public_path($this->img_url));

        // ----------------------

        // $path = public_path($img_url);

        // $newImage = Image::make($path,[

        //  'width' => 800,
        //  'height' => 400,
        //  'crop' => true,

        // ]);

        // $newImage->save($path);


    }
}
